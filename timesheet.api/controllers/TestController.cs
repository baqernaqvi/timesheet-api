﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace timesheet.api.controllers
{
    [Route("api/Test/")]
    [ApiController]
    public class TestController : ControllerBase
    {
        [HttpGet("Get")]
        public IActionResult Get()
        {
            return new ObjectResult(true);

        }
    }
}