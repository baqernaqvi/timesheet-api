﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.business.Util;
using timesheet.model;
using timesheet.model.mappers;

namespace timesheet.api.controllers
{
    [Route("api/v1/timelog")]
    [ApiController]
    public class TimeLogController : ControllerBase
    {
        private readonly TimeLogService logService;
        public TimeLogController(TimeLogService logservice)
        {
            this.logService = logservice;
        }

        [HttpGet("Get")]
        public IActionResult Get(int employeeId, int week)
        {
            var logs = this.logService.GetEmpLog(employeeId, week);
            var startingDate = DateTime.Now.AddDays(-6 * 1);


            var newLogs = new List<TimeLog_Local>();
            TimeLog_Local tempo = null;

            for (var i = 0; i < 7; i++)
            {
                var currentday = startingDate.AddDays(i).Date;
                var dayName = currentday.ToString("dddd");

                var reco = logs.FirstOrDefault(l => l.Day.Date == currentday);
                if (reco == null)
                {
                    tempo = new TimeLog_Local
                    {
                        DayName = dayName,
                        SortOrder= Common.GetDaySort(dayName)
                    };
                }
                else
                {
                    reco.DayName = dayName;
                    reco.SortOrder = Common.GetDaySort(dayName);
                    tempo = reco;
                }
                newLogs.Add(tempo);
            }
            return new ObjectResult(logs);
        }


        [HttpPost("Add")]
        public IActionResult Set(TimeLog source)
        {
           var response = this.logService.AddEmpLog(source);
            return new ObjectResult(response);

        }





    }
    }