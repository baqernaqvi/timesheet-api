﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;

namespace timesheet.api.controllers
{
    [Route("api/v1/task")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        #region Config Data
        private readonly TaskService taskService;
        public TaskController(TaskService taskService)
        {
            this.taskService = taskService;
        } 
        #endregion

        [HttpGet("getAll")]
        public IActionResult GetAll(string text)
        {
            var tasks = this.taskService.GetTasks();
            return new ObjectResult(tasks);
        }
    }
}