# README
## NAME
TimeSheet
## DESCRIPTION
A simple **TimeSheet** application used to record the time logs of employees. Its the backend of the **TimeSheet** app developed in **.Net Core**

## INSTALLATION

### Cloning repository
Open terminal clone this repository by running the command below
```bash
git clone https://baqernaqvi@bitbucket.org/baqernaqvi/timesheet-api.git
```

### Navigate to the directory
Navigate to the folder **timesheet-api** and open **timesheet.api.sln** with **Microsoft Visual Studio**

### Run Project
Select **timesheet.api** from the dropdown Startup Projects. Right next to it select IIS Express from dropdown and click on :arrow_forward: After successful run, you can use endpoints. Base URL is <https://localhost:44391/api/v1>

## USAGE
It provides some endpoints for the frontend. For Example: 
1.	Get all users
2.	Get all tasks
3.	Get time logs of a specific user against a week
4.	Add a time log against a specific user