﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.data;
using timesheet.model;
using Microsoft.EntityFrameworkCore;
using timesheet.model.mappers;

namespace timesheet.business
{
    public class TimeLogService
    {
        #region Config Data
        public TimesheetDb db { get; }
        public TimeLogService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }
        #endregion

        public List<TimeLog_Local> GetEmpLog(int empId, int week)
        {
            var logs = db.Logs.Where(log => log.EmployeeId == empId && log.Day > DateTime.Now.AddDays(week*-7)).Include("Task").Include("Employee");
            if (logs.Any())
            {
                return logs.Select(o => o.Mapper()).ToList();
            }
            return new List<TimeLog_Local>();
        }


        public bool AddEmpLog(TimeLog souce)
        {
            try
            {
                db.Logs.Add(souce);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
