﻿using System;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService
    {
        #region Config Data
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        } 
        #endregion

        public IQueryable<Employee> GetEmployees()
        {
            return this.db.Employees;
        }
    }
}
