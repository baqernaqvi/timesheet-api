﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.business.Util
{
    public static class Common
    {
        public static int GetDaySort(string dayname)
        {
            dayname = dayname.ToLower();
            switch (dayname)
            {
                case "sunday":
                    return 0;
                case "monday":
                    return 1;
                case "tuesday":
                    return 2;
                case "wednesday":
                    return 3;
                case "thursday":
                    return 4;
                case "friday":
                    return 5;
                case "saturday":
                    return 6;

                default:
                    return 0;
            }
        }
    }
}
