﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model.mappers
{
    public class TimeLog_Local
    {
        public int Id { get; set; }
        public DateTime Day { get; set; }
        public string DayName { get; set; }
        public int SortOrder { get; set; }



        public int Hours { get; set; }

        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }


        public int TaskId { get; set; }
        public string TaskName { get; set; }

    }

    public static class TimeMappe
    {
        public static TimeLog_Local Mapper(this TimeLog source)
        {
            return new TimeLog_Local
            {
                Id = source.Id,
                EmployeeId= source.EmployeeId,
                EmployeeName= source.Employee?.Name,
                Hours= source.Hours,
                TaskId= source.TaskId,
                TaskName= source.Task?.Name,
                Day= source.Day
            };
        }

    }
}
